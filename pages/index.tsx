import type { NextPage } from "next"
import { useRouter } from "next/router"
import styles from "../styles/Home.module.css"

const Home: NextPage = () => {
  const router = useRouter()
  const about = () => {
    router.push("/about")
  }
  const reloadr = () => {
    router.reload();
  }
  return (<>
    <div className={styles.container}>
      home page view
      <br />
      <button onClick={about}>about-page</button>
      <button onClick={reloadr}>reload</button>
    </div>
  </>
  )
}

export default Home
