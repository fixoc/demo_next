import { useRouter } from "next/router"
import styles from "../styles/Home.module.css"

const About = () => {
    const router = useRouter()
    const back = () => {
        router.push("/");
    }
    const reloadr = () => {
        router.reload();
    }
    return (
        <>
            <div className={styles.container}>
                this is about page <br />
                < br />
                <button onClick={back}>back to home page</button>
                <button onClick={reloadr}>reload</button>
                <br />
            </div>
        </>
    )
}
export default About
